package vlad;


import vlad.Controller.FamilyController;
import vlad.DAO.CollectionFamilyDao;
import vlad.Humans.Family;
import vlad.Humans.Human;
import vlad.Humans.Man;
import vlad.Humans.Woman;
import vlad.DAO.FamilyDao;
import vlad.Pets.Dog;
import vlad.Pets.DomesticCat;
import vlad.Pets.Pet;
import vlad.Pets.RoboCat;
import vlad.Service.FamilyService;

import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        Human child1 = new Man("Vlad", "Lietun");
        child1.setBirthDate("16.03.1995");
        Human child2 = new Man("Nick", "Bubalo");
        Human woman = new Woman("Elena", "Lietun");
        Human man = new Man("Dima", "Lietun");
        Family family = new Family(woman, man);
        family.addChild(child1);

        Human woman1 = new Woman("Kate", "Bubalo");
        Human man1 = new Man("Bob", "Bubalo");
        Family family1 = new Family(woman1, man1);
        family1.addChild(child2);

        FamilyDao familyDao = new CollectionFamilyDao();
        familyDao.saveFamily(family);
        familyDao.saveFamily(family1);

        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        System.out.println(familyController.countFamiliesWithMemberNumber(3));

        System.out.println(family.getChildren().get(0).getBirthDate());
        System.out.println(family.getChildren().get(0).describeAge());

        Human human = new Man("sadad", "saadas", "10.11.1999", 10);

        System.out.println(human);
        System.out.println(child1.toString());

    }
}


