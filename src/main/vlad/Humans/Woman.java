package vlad.Humans;

import vlad.Pets.Pet;

import java.util.Map;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname) {
        super(name, surname);
    }

    public Woman(String name, String surname, String birthDate, int iq, Map<String, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public Woman(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    void makeup() {
        System.out.println("подкраситься");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Приветик " + pet.getNickname());
    }
}

