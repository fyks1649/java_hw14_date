package vlad.Humans;

import vlad.Pets.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.*;

public abstract class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Family family;
    private Map<String, String> schedule;

    public Human() {
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, String birthDate, int iq, Map<String, String> schedule) {
        this.name = name;
        this.surname = surname;
        simpleDateFormat(birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        simpleDateFormat(birthDate);
        this.iq = iq;
    }

    public abstract void greetPet(Pet pet);

    public void describePet(Pet pet) {
        System.out.println("У меня есть " + pet.getSpecies() + ", ему " + pet.getAge() + " года, он очень хитрый!");
    }

    public void finalize() {
        System.out.println(name + " " + surname + ". This human is removed.");
    }


    //Math method
//    public String describeAge() {
//
//        long dateCurrent = System.currentTimeMillis();
//        long result = dateCurrent - this.birthDate;
//
//        long ost = result % 31536000000L;
//        result = result - ost;
//        result = result / 31536000000L;
//
//        long resultMonth = ost / 2592000000L;
//        long resultDay = resultMonth - resultMonth / 86400000L;
//
//        return result + " года " + resultMonth + " месяцев " + resultDay + " дней";
//    }

    public String describeAge() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(this.birthDate));
        LocalDate birthDate = LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        LocalDate now = LocalDate.now();
        Period period = Period.between(birthDate, now);
        return getName() + " " + getSurname() + ", тебе " +  period.getYears() + " года, " + period.getMonths() + " месяце, " + period.getDays() + " дней.";

    }

    public void simpleDateFormat(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = null;
        try {
            date = (Date) simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date != null;
        this.birthDate = date.getTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate);
    }

    public String toString() {
        if (birthDate == 0 && iq == 0 && schedule == null){
            return "Human{name=" + name +
                    ", surname=" + surname + "}";
        } else {
            return "Human{name=" + name +
                    ", surname=" + surname +
                    ", birthDate=" + getBirthDate() +
                    ", iq=" + iq +
                    ", schedule=" + schedule + "}";
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(String string) {
        simpleDateFormat(string);
    }

    public String getBirthDate() {
        Date date = new Date(birthDate);
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
        return dt.format(date);
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    public Family getFamily() { return family; }

    public void setFamily(Family family) { this.family = family; }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }
}

