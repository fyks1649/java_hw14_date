package vlad.Controller;

import vlad.Humans.Family;
import vlad.Humans.Human;
import vlad.Pets.Pet;
import vlad.Service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        return familyService.getFamiliesBiggerThan(number);
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyService.getFamiliesLessThan(number);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human human1, Human human2) {
        familyService.createNewFamily(human1, human2);
    }

    public void deleteFamily(int index) {
        familyService.deleteFamily(index);
    }

    public Family bornChild(Family family, String nameMan, String nameWoman) {
        return familyService.bornChild(family, nameMan, nameWoman);
    }

    public Family adoptChild(Family family, Human human) {
        return familyService.adoptChild(family, human);
    }

//    public void deleteAllChildrenOlderThen(int age) {
//        familyService.deleteAllChildrenOlderThen(age);
//    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int indexFamily) {
        return familyService.getFamilyById(indexFamily);
    }

    public Set<Pet> getPets(int indexFamily) {
        return familyService.getPets(indexFamily);
    }

    public void addPet(int indexFamily, Pet pet) {
        familyService.addPet(indexFamily, pet);
    }
}
