package vlad.Service;

import vlad.DAO.FamilyDao;
import vlad.Enum.Species;
import vlad.Humans.Family;
import vlad.Humans.Human;
import vlad.Humans.Man;
import vlad.Humans.Woman;
import vlad.Pets.Dog;
import vlad.Pets.DomesticCat;
import vlad.Pets.Pet;

import java.util.*;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        String result = "";
        System.out.println("List of all families: ");
        for (Family family : familyDao.getAllFamilies()) {
            result = (familyDao.getAllFamilies().indexOf(family) + 1) + ". " + family;
            System.out.println(result);
        }
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        List<Family> families = new ArrayList<>();
        int count = 1;
        System.out.println("Family bigger than " + number + " people:");
        for (Family family : familyDao.getAllFamilies()) {
            if (number < family.countFamily()) {
                families.add(family);
            }
        }
        return families;
    }

    public List<Family> getFamiliesLessThan(int number) {
        List<Family> families = new ArrayList<>();
        int count = 1;
        System.out.println("Family less than " + number + " people:");
        for (Family family : familyDao.getAllFamilies()) {
            if (number > family.countFamily()) {
                families.add(family);
            }
        }
        return families;
    }

    public int countFamiliesWithMemberNumber(int number) {
        int count = 0;
        System.out.println("Family number of people equal " + number + ":");
        for (Family family : familyDao.getAllFamilies()) {
            if (number == family.countFamily()) {
                count++;
            }
        }
        return count;
    }

    public void createNewFamily(Human human1, Human human2) {
        familyDao.saveFamily(new Family(human1, human2));
    }

    public void deleteFamily(int index) {
        if (index - 1 < 0 || index - 1 > familyDao.getAllFamilies().size()-1) {
        } else familyDao.getAllFamilies().remove(index - 1);
    }

    public Family bornChild(Family family, String nameMan, String nameWoman) {
        int random = (int) (Math.random() * 2);
        Human man = new Man(nameMan, family.getFather().getSurname());
        Human woman = new Woman(nameWoman, family.getFather().getSurname());
        if (random == 0) {
            family.addChild(man);
            familyDao.saveFamily(family);
        } else {
            family.addChild(woman);
            familyDao.saveFamily(family);
        }
        return family;
    }

    public Family adoptChild(Family family, Human human) {
        human.setFamily(family);
        human.setSurname(family.getFather().getSurname());
        family.addChild(human);
        familyDao.saveFamily(family);
        return family;
    }

//    public void deleteAllChildrenOlderThen(int age) {
//        int resultAge;
//
//        for (Family family : familyDao.getAllFamilies()) {
//            Iterator iterator = family.getChildren().iterator();
//            while (iterator.hasNext()) {
//                Human child = (Human) iterator.next();
//                resultAge = 2019 - child.getBirthDate();
//                if (!(child.getYear() == 0)) {
//                    if (resultAge > age) {
//                        iterator.remove();
//                        familyDao.saveFamily(family);
//                    }
//                }
//            }
//        }
//    }

    public int count() {
        int result = 0;
        for (Family aFamily : familyDao.getAllFamilies()) {
            result++;
        }
        return result;
    }

    public Family getFamilyById(int indexFamily) {
        System.out.println("Family by id " + indexFamily + ":");
        return familyDao.getAllFamilies().get(indexFamily - 1);
    }

    public Set<Pet> getPets(int indexFamily) {
        System.out.println("Pets family by index " + indexFamily + ":");
        return familyDao.getFamilyByIndex(indexFamily - 1).getPet();
    }

    public void addPet(int indexFamily, Pet pet) {
        Family familyByIndex = familyDao.getFamilyByIndex(indexFamily - 1);
        familyByIndex.getPet().add(pet);
        familyDao.saveFamily(familyByIndex);
    }
}
