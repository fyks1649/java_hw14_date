package vlad.Pets;

import vlad.Interfaces.Foul;
import vlad.Enum.Species;

import java.util.Set;

public class Dog extends Pet implements Foul {

    public Dog() {
        this.species = Species.DOG;
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DOG;
    }

    @Override
    public void respond() {
        System.out.println("Gav gav");
    }

    @Override
    public void foul() {
        System.out.println("Play with me owner");
    }
}
